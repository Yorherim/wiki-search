import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunkMiddleware, { ThunkAction } from "redux-thunk";

import { SearchActionsTypes, searchReducer } from "./reducers/search-reducer";

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    search: searchReducer,
});

export type AppRootStateType = ReturnType<typeof rootReducer>;
export type AppRootActionsType = SearchActionsTypes;
export type ThunkType = ThunkAction<
    void,
    AppRootStateType,
    unknown,
    AppRootActionsType
>;

export const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunkMiddleware))
);
