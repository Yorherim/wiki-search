import {
    searchActions,
    searchReducer,
    SearchStateType,
    WikiSearchElementType,
} from "./search-reducer";

const {
    setResults,
    resetOldElements,
    toggleIsLoading,
    rememberSearchText,
    changeCurrentPage,
} = searchActions;

let state: SearchStateType;

beforeEach(() => {
    state = {
        results: [
            {
                ns: 0,
                title: "Craig Noone",
                pageid: 18846922,
                size: 24240,
                wordcount: 1889,
                snippet:
                    '<span class="searchmatch">Craig</span> Stephen <span class="searchmatch">Noone</span> (born 17 November 1987) is an English professional footballer who plays as a winger for A-League club Macarthur FC. He has also played',
                timestamp: "2021-07-10T06:40:38Z",
            },
            {
                ns: 0,
                title: "Noone",
                pageid: 32906333,
                size: 729,
                wordcount: 123,
                snippet:
                    '<span class="searchmatch">Noone</span> is a surname. Notable people with the surname include: <span class="searchmatch">Craig</span> <span class="searchmatch">Noone</span> (born 1987), English football midfielder Eímear <span class="searchmatch">Noone</span>, Irish conductor and composer',
                timestamp: "2021-03-04T10:25:20Z",
            },
            {
                ns: 0,
                title: "A-League transfers for 2021–22 season",
                pageid: 67921107,
                size: 35941,
                wordcount: 1204,
                snippet:
                    'signing City A-League star&quot;. FTBL. 14 June 2021. &quot;Championship Winner <span class="searchmatch">Craig</span> <span class="searchmatch">Noone</span> Joins The Bulls&quot;. Macarthur FC. 11 July 2021. Rollo, Phillip (15 June',
                timestamp: "2021-07-16T05:32:43Z",
            },
            {
                ns: 0,
                title: "2021–22 A-League",
                pageid: 68008027,
                size: 12857,
                wordcount: 459,
                snippet:
                    'work ahead\': Popovic named Victory coach&quot;. The Age. Kerry, <span class="searchmatch">Craig</span> (3 June 2021). &quot;A-League: <span class="searchmatch">Craig</span> Deans to step down as coach of Newcastle Jets&quot;. The Newcastle',
                timestamp: "2021-07-16T05:28:03Z",
            },
        ],
        rememberSearchText: "",
        isLoading: false,
        currentPage: 1,
    };
});

test("old results should be reseted", () => {
    expect(state.results.length).toBe(4);
    const endState = searchReducer(state, resetOldElements());
    expect(endState.results.length).toBe(0);
});

test("new results should be setted", () => {
    const newResults: Array<WikiSearchElementType> = [
        {
            ns: 0,
            title: "ABC",
            pageid: 67921107,
            size: 35941,
            wordcount: 1204,
            snippet:
                'signing City A-League star&quot;. FTBL. 14 June 2021. &quot;Championship Winner <span class="searchmatch">Craig</span> <span class="searchmatch">Noone</span> Joins The Bulls&quot;. Macarthur FC. 11 July 2021. Rollo, Phillip (15 June',
            timestamp: "2021-07-16T05:32:43Z",
        },
        {
            ns: 0,
            title: "CBA",
            pageid: 68008027,
            size: 12857,
            wordcount: 459,
            snippet:
                'work ahead\': Popovic named Victory coach&quot;. The Age. Kerry, <span class="searchmatch">Craig</span> (3 June 2021). &quot;A-League: <span class="searchmatch">Craig</span> Deans to step down as coach of Newcastle Jets&quot;. The Newcastle',
            timestamp: "2021-07-16T05:28:03Z",
        },
    ];

    const endState = searchReducer(state, setResults(newResults));

    expect(endState.results.length).toBe(6);
});

test("isLoading should be toggled", () => {
    const endState = searchReducer(state, toggleIsLoading(true));
    expect(endState.isLoading).toBeTruthy();

    const endState2 = searchReducer(endState, toggleIsLoading(false));
    expect(endState2.isLoading).toBeFalsy();
});

test("search text should be remembered", () => {
    const searchText = "Hello";
    const endState = searchReducer(state, rememberSearchText(searchText));
    expect(endState.rememberSearchText).toBe(searchText);

    const searchText2 = "Privet";
    const endState2 = searchReducer(endState, rememberSearchText(searchText2));
    expect(endState2.rememberSearchText).toBe(searchText2);
});

test("currentPage should be changed", () => {
    const endState = searchReducer(state, changeCurrentPage(2));

    expect(endState.currentPage).toBe(2);
});
