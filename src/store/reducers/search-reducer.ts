import { ThunkType } from "../store";
import { searchAPI } from "../../api/api";

export type WikiSearchElementType = {
    ns: number;
    title: string;
    pageid: number;
    size: number;
    wordcount: number;
    snippet: string;
    timestamp: string;
};
export type SearchStateType = {
    rememberSearchText: string;
    results: Array<WikiSearchElementType>;
    isLoading: boolean;
    currentPage: number;
};
type InferValueTypes<T> = T extends { [key: string]: infer U } ? U : never;
export type SearchActionsTypes = ReturnType<
    InferValueTypes<typeof searchActions>
>;

const searchState: SearchStateType = {
    rememberSearchText: "",
    results: [],
    isLoading: false,
    currentPage: 1,
};

console.log(searchState.currentPage);

export const searchReducer = (
    state = searchState,
    action: SearchActionsTypes
) => {
    switch (action.type) {
        case "SET-RESULTS":
            return { ...state, results: [...state.results, ...action.results] };
        case "RESET": {
            const newState = { ...state, results: [...state.results] };
            newState.results = [];
            return newState;
        }
        case "REMEMBER-SEARCH-TEXT":
            return { ...state, rememberSearchText: action.text };
        case "TOGGLE-IS-LOADING":
            return { ...state, isLoading: action.toggle };
        case "CHANGE-PAGE":
            return { ...state, currentPage: action.page };
        default:
            return state;
    }
};

// actions creators
export const searchActions = {
    setResults: (results: Array<WikiSearchElementType>) => ({
        type: "SET-RESULTS" as const,
        results,
    }),
    resetOldElements: () => ({
        type: "RESET" as const,
    }),
    rememberSearchText: (text: string) => ({
        type: "REMEMBER-SEARCH-TEXT" as const,
        text,
    }),
    toggleIsLoading: (toggle: boolean) => ({
        type: "TOGGLE-IS-LOADING" as const,
        toggle,
    }),
    changeCurrentPage: (page: number) => ({
        type: "CHANGE-PAGE" as const,
        page,
    }),
};

// thunks creators
const { getResults } = searchAPI;
const { setResults, resetOldElements, rememberSearchText, toggleIsLoading } =
    searchActions;

export const fetchResultsTC =
    (srsearch: string): ThunkType =>
    async (dispatch) => {
        try {
            dispatch(toggleIsLoading(true));
            dispatch(resetOldElements());
            dispatch(rememberSearchText(srsearch));
            const results = await getResults(srsearch);
            if (results.searchinfo.totalhits > 0) {
                dispatch(setResults(results.search));
            } else {
                alert("Results not found :(");
            }
            dispatch(toggleIsLoading(false));
        } catch (err) {
            console.error(err.messages);
            throw new Error(err);
        }
    };
