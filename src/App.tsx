import React from "react";
import { useSelector } from "react-redux";

import { Container, LinearProgress } from "@material-ui/core";
import "./App.scss";

import Search from "./components/Search/Search";
import { AppRootStateType } from "./store/store";
import { WikiSearchElementType } from "./store/reducers/search-reducer";
import TableResults from "./components/Table/TableResults";

function App() {
    const currentPage = useSelector<AppRootStateType, number>(
        (state) => state.search.currentPage
    );
    const results = useSelector<AppRootStateType, Array<WikiSearchElementType>>(
        (state) => state.search.results
    );
    const isLoading = useSelector<AppRootStateType, boolean>(
        (state) => state.search.isLoading
    );

    return (
        <div className="App">
            {isLoading && (
                <LinearProgress
                    style={{
                        position: "absolute",
                        width: "100%",
                        top: 0,
                    }}
                />
            )}
            <Container style={{ maxWidth: 960 }} fixed className={"container"}>
                <h1>Wikipedia Search</h1>
                <Search />
                {results.length !== 0 ? (
                    <TableResults results={results} currentPage={currentPage} />
                ) : (
                    ""
                )}
            </Container>
        </div>
    );
}

export default App;
