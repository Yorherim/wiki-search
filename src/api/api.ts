import axios from "axios";

const instance = axios.create({
    baseURL: `https://en.wikipedia.org/w/api.php?`,
});
const params = {
    origin: "*",
    action: "query",
    list: "search",
    format: "json",
    srsearch: "",
    srlimit: 30,
};

export const searchAPI = {
    getResults(srsearch: string) {
        params.srsearch = srsearch.trim().replace(/ /g, "&");
        return instance.get(``, { params }).then((res) => res.data.query);
    },
};
